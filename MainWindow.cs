﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace docsign
{
    public partial class MainWindow : Form
    {
        private SignDocument signForm;

        private VerifySignature verifyForm;

        private AboutDialog about;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void signNewButton_Click(object sender, EventArgs e)
        {
            signForm = new SignDocument();
            signForm.ShowDialog(this);
        }

        private void verifySignatureButton_Click(object sender, EventArgs e)
        {
            verifyForm = new VerifySignature();
            verifyForm.ShowDialog(this);
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void about_Click(object sender, EventArgs e)
        {
            about = new AboutDialog();
            about.ShowDialog(this);
        }
    }
}
