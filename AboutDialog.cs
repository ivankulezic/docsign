﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace docsign
{
    partial class AboutDialog : Form
    {
        public AboutDialog()
        {
            InitializeComponent();
            this.Text = String.Format("About {0}", AssemblyTitle);
            this.labelProductName.Text = AssemblyProduct;
            this.labelVersion.Text = String.Format("Version {0}", AssemblyVersion);
            this.labelCopyright.Text = AssemblyCopyright;
            this.labelCompanyName.Text = AssemblyCompany;
            this.textBoxDescription.Text = AssemblyDescription;
        }

        #region Assembly Attribute Accessors

        public string AssemblyTitle
        {
            get
            {
                return "DocSign";
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return "1.0.0";
            }
        }

        public string AssemblyDescription
        {
            get
            {
                return "This project is proof of concept application as part of master thesis at University of Belgrade, School of Electrical Engineering. It demonstrates how to utilize certificates and PKI from smart cards to sign any document as well as verify signature created by this application.";
            }
        }

        public string AssemblyProduct
        {
            get
            {
                return "Document Signer";
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                return "Copyright ©2020 Ivan Kulezic";
            }
        }

        public string AssemblyCompany
        {
            get
            {
                return "DocSign";
            }
        }
        #endregion
    }
}
