actor Korisnik
participant MainWindow
participant SignDocument
participant MessageBox
participant FileExplorer

Korisnik->MainWindow: pokreni program
activate MainWindow
Korisnik<--MainWindow: prikaži UI
Korisnik->MainWindow: odabir opcije potpiši dokument
MainWindow->SignDocument: napravi formu
activate SignDocument
Korisnik<--SignDocument: prikaži UI
Korisnik->SignDocument:odabir dokumenta i izlazne lokacije
SignDocument->Korisnik:prikaži UI za unos PIN-a
SignDocument<--Korisnik: unos PIN-a
SignDocument->SignDocument: generisanje potpisa
alt potpis uspešan
SignDocument->MessageBox: prikaži poruku o uspešnosti operacije
activate MessageBox
MessageBox->Korisnik: prikaži UI
MessageBox<--Korisnik: zatvori UI
deactivate MessageBox
SignDocument->FileExplorer:prikaži sadržaj izlazne lokacije
activate FileExplorer
SignDocument->SignDocument: obriši formu
deactivate SignDocument
FileExplorer->Korisnik: prikaži UI
FileExplorer<--Korisnik: zatvori UI
deactivate FileExplorer
else potpis neuspešan
activate SignDocument
SignDocument->MessageBox: prikaži poruku o neuspešnosti operacije
activate MessageBox
MessageBox->Korisnik: prikaži UI
MessageBox<--Korisnik: zatvori UI
deactivate MessageBox
SignDocument->SignDocument: obriši formu
deactivate SignDocument
end
Korisnik->MainWindow: zatvori program
MainWindow->MainWindow: obriši formu
deactivate MainWindow
