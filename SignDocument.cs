﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace docsign
{
    public partial class SignDocument : Form
    {
        public SignDocument()
        {
            InitializeComponent();
        }

        private void browseFile_Click(object sender, EventArgs e)
        {
            DialogResult result = fileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                fileLocation.Text = fileDialog.FileName;
            }
            enableSignButton();
        }

        private void browseFiles_Click(object sender, EventArgs e)
        {
            DialogResult result = folderDialog.ShowDialog();

            if(result == DialogResult.OK)
            {
                outputLocation.Text = folderDialog.SelectedPath;
            }
            enableSignButton();
        }

        private void signButton_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] document = File.ReadAllBytes(fileLocation.Text);
                byte[] hash = SHA256.Create().ComputeHash(document);

                CspParameters cspParams = new CspParameters(1, "Microsoft Base Smart Card Crypto Provider");
                cspParams.Flags = CspProviderFlags.UseDefaultKeyContainer;

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cspParams);
                byte[] signature = rsa.SignHash(hash, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
                File.WriteAllBytes(Path.Combine(outputLocation.Text, "signature.sign"), signature);

                string publicKeySerialized = rsa.ToXmlString(false);
                X509Store x509Store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                x509Store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                foreach (X509Certificate2 cert in x509Store.Certificates)
                {
                    if ((cert.PublicKey.Key.ToXmlString(false) == publicKeySerialized) && cert.HasPrivateKey)
                    {
                        byte[] certFile = cert.Export(X509ContentType.Cert);
                        File.WriteAllBytes(Path.Combine(outputLocation.Text, "certificate.cer"), certFile);
                        break;
                    }
                }

                File.Copy(fileLocation.Text, Path.Combine(outputLocation.Text, Path.GetFileName(fileLocation.Text)), true);
            } catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                MessageBox.Show(this, "Failed to sign the document", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show(this, "Document signed successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Process.Start("explorer.exe", "/open, " + outputLocation.Text);
            this.Dispose();
        }

        private void enableSignButton()
        {
            if (String.IsNullOrEmpty(outputLocation.Text) || String.IsNullOrEmpty(fileLocation.Text))
            {
                signButton.Enabled = false;
                return;
            }
            signButton.Enabled = true;
        }
    }
}
