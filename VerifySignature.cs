﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace docsign
{
    public partial class VerifySignature : Form
    {
        public VerifySignature()
        {
            InitializeComponent();
        }

        private void browseDocumentButton_Click(object sender, EventArgs e)
        {
            DialogResult result = fileBrowser.ShowDialog();

            if (result == DialogResult.OK)
            {
                documentLocation.Text = fileBrowser.FileName;
            }
            enableVerifyButton();
        }

        private void browseSignatureButton_Click(object sender, EventArgs e)
        {
            DialogResult result = fileBrowser.ShowDialog();

            if (result == DialogResult.OK)
            {
                signatureLocation.Text = fileBrowser.FileName;
            }
            enableVerifyButton();
        }

        private void browseCertificateButton_Click(object sender, EventArgs e)
        {
            DialogResult result = fileBrowser.ShowDialog();

            if (result == DialogResult.OK)
            {
                certificateLocation.Text = fileBrowser.FileName;
            }
            enableVerifyButton();
        }

        private void verifySignatureButton_Click(object sender, EventArgs e)
        {
            byte[] document = File.ReadAllBytes(documentLocation.Text);
            byte[] hash = SHA256.Create().ComputeHash(document);
            byte[] signature = File.ReadAllBytes(signatureLocation.Text);
            X509Certificate2 cert = new X509Certificate2(certificateLocation.Text);

            if (!cert.Verify())
            {
                MessageBox.Show(this, "Certificate couldn't be verified.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            bool status = cert.GetRSAPublicKey().VerifyHash(hash, signature, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            if (status)
            {
                MessageBox.Show(this, "Signature is valid", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else
            {
                MessageBox.Show(this, "Signature is invalid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void enableVerifyButton()
        {
            if (String.IsNullOrEmpty(certificateLocation.Text) || String.IsNullOrEmpty(documentLocation.Text) || String.IsNullOrEmpty(signatureLocation.Text))
            {
                verifySignatureButton.Enabled = false;
                return;
            }
            verifySignatureButton.Enabled = true;
        }
    }
}
