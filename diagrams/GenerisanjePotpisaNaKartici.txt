actor Korisnik

activate SignDocument
Korisnik->SignDocument: potpiši dokument
SignDocument->SignDocument: sračunaj heš vredost dokumenta
SignDocument->CryptoServiceProvider: dohvati provajdera za pristup kartici
activate CryptoServiceProvider
SignDocument<--CryptoServiceProvider: provajder
SignDocument->CryptoServiceProvider: potpiši heš vrednost dokumenta
CryptoServiceProvider->SmartCard: iniciraj potpis
activate SmartCard
SmartCard->CryptoServiceProvider: unesi PIN
CryptoServiceProvider->SignDocument: unesi PIN
SignDocument->Korisnik:prikaži UI za unos PIN-a
SignDocument<--Korisnik: unos PIN-a
CryptoServiceProvider<--SignDocument: prosledi PIN
SmartCard<--CryptoServiceProvider: prosledi PIN
SmartCard->SmartCard: generisanje potpisa
alt potpis uspešan
CryptoServiceProvider<--SmartCard: potpis
SignDocument<--CryptoServiceProvider: potpis
else potpis neuspešan
CryptoServiceProvider<--SmartCard: izuzetak
SignDocument<--CryptoServiceProvider: izuzetak
end
