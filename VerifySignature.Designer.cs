﻿namespace docsign
{
    partial class VerifySignature
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VerifySignature));
            this.documentLocation = new System.Windows.Forms.TextBox();
            this.signatureLocation = new System.Windows.Forms.TextBox();
            this.certificateLocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.browseDocumentButton = new System.Windows.Forms.Button();
            this.browseSignatureButton = new System.Windows.Forms.Button();
            this.browseCertificateButton = new System.Windows.Forms.Button();
            this.verifySignatureButton = new System.Windows.Forms.Button();
            this.fileBrowser = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // documentLocation
            // 
            this.documentLocation.Location = new System.Drawing.Point(28, 55);
            this.documentLocation.Name = "documentLocation";
            this.documentLocation.ReadOnly = true;
            this.documentLocation.Size = new System.Drawing.Size(365, 23);
            this.documentLocation.TabIndex = 0;
            // 
            // signatureLocation
            // 
            this.signatureLocation.Location = new System.Drawing.Point(28, 110);
            this.signatureLocation.Name = "signatureLocation";
            this.signatureLocation.ReadOnly = true;
            this.signatureLocation.Size = new System.Drawing.Size(365, 23);
            this.signatureLocation.TabIndex = 1;
            // 
            // certificateLocation
            // 
            this.certificateLocation.Location = new System.Drawing.Point(28, 168);
            this.certificateLocation.Name = "certificateLocation";
            this.certificateLocation.ReadOnly = true;
            this.certificateLocation.Size = new System.Drawing.Size(365, 23);
            this.certificateLocation.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Document location:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Signature location:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Certificate location:";
            // 
            // browseDocumentButton
            // 
            this.browseDocumentButton.Location = new System.Drawing.Point(423, 54);
            this.browseDocumentButton.Name = "browseDocumentButton";
            this.browseDocumentButton.Size = new System.Drawing.Size(75, 23);
            this.browseDocumentButton.TabIndex = 6;
            this.browseDocumentButton.Text = "Browse";
            this.browseDocumentButton.UseVisualStyleBackColor = true;
            this.browseDocumentButton.Click += new System.EventHandler(this.browseDocumentButton_Click);
            // 
            // browseSignatureButton
            // 
            this.browseSignatureButton.Location = new System.Drawing.Point(423, 110);
            this.browseSignatureButton.Name = "browseSignatureButton";
            this.browseSignatureButton.Size = new System.Drawing.Size(75, 23);
            this.browseSignatureButton.TabIndex = 7;
            this.browseSignatureButton.Text = "Browse";
            this.browseSignatureButton.UseVisualStyleBackColor = true;
            this.browseSignatureButton.Click += new System.EventHandler(this.browseSignatureButton_Click);
            // 
            // browseCertificateButton
            // 
            this.browseCertificateButton.Location = new System.Drawing.Point(423, 168);
            this.browseCertificateButton.Name = "browseCertificateButton";
            this.browseCertificateButton.Size = new System.Drawing.Size(75, 23);
            this.browseCertificateButton.TabIndex = 8;
            this.browseCertificateButton.Text = "Browse";
            this.browseCertificateButton.UseVisualStyleBackColor = true;
            this.browseCertificateButton.Click += new System.EventHandler(this.browseCertificateButton_Click);
            // 
            // verifySignatureButton
            // 
            this.verifySignatureButton.Enabled = false;
            this.verifySignatureButton.Location = new System.Drawing.Point(186, 221);
            this.verifySignatureButton.Name = "verifySignatureButton";
            this.verifySignatureButton.Size = new System.Drawing.Size(146, 39);
            this.verifySignatureButton.TabIndex = 9;
            this.verifySignatureButton.Text = "Verify signature";
            this.verifySignatureButton.UseVisualStyleBackColor = true;
            this.verifySignatureButton.Click += new System.EventHandler(this.verifySignatureButton_Click);
            // 
            // VerifySignature
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 283);
            this.Controls.Add(this.verifySignatureButton);
            this.Controls.Add(this.browseCertificateButton);
            this.Controls.Add(this.browseSignatureButton);
            this.Controls.Add(this.browseDocumentButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.certificateLocation);
            this.Controls.Add(this.signatureLocation);
            this.Controls.Add(this.documentLocation);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "VerifySignature";
            this.Text = "DocSign - Verify Signature";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox documentLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button browseDocumentButton;
        private System.Windows.Forms.Button browseSignatureButton;
        private System.Windows.Forms.Button browseCertificateButton;
        private System.Windows.Forms.Button verifySignatureButton;
        private System.Windows.Forms.OpenFileDialog fileBrowser;
        private System.Windows.Forms.TextBox signatureLocation;
        private System.Windows.Forms.TextBox certificateLocation;
    }
}