﻿namespace docsign
{
    partial class SignDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignDocument));
            this.fileDialog = new System.Windows.Forms.OpenFileDialog();
            this.fileLocation = new System.Windows.Forms.TextBox();
            this.browseFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.outputLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.folderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.browseFiles = new System.Windows.Forms.Button();
            this.signButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fileLocation
            // 
            this.fileLocation.Location = new System.Drawing.Point(27, 38);
            this.fileLocation.Name = "fileLocation";
            this.fileLocation.ReadOnly = true;
            this.fileLocation.Size = new System.Drawing.Size(357, 23);
            this.fileLocation.TabIndex = 0;
            // 
            // browseFile
            // 
            this.browseFile.Location = new System.Drawing.Point(413, 37);
            this.browseFile.Name = "browseFile";
            this.browseFile.Size = new System.Drawing.Size(75, 23);
            this.browseFile.TabIndex = 1;
            this.browseFile.Text = "Browse";
            this.browseFile.UseVisualStyleBackColor = true;
            this.browseFile.Click += new System.EventHandler(this.browseFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Location of file to be encrypted:";
            // 
            // outputLocation
            // 
            this.outputLocation.Location = new System.Drawing.Point(27, 97);
            this.outputLocation.Name = "outputLocation";
            this.outputLocation.ReadOnly = true;
            this.outputLocation.Size = new System.Drawing.Size(357, 23);
            this.outputLocation.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Output location:";
            // 
            // browseFiles
            // 
            this.browseFiles.Location = new System.Drawing.Point(413, 97);
            this.browseFiles.Name = "browseFiles";
            this.browseFiles.Size = new System.Drawing.Size(75, 23);
            this.browseFiles.TabIndex = 5;
            this.browseFiles.Text = "Browse";
            this.browseFiles.UseVisualStyleBackColor = true;
            this.browseFiles.Click += new System.EventHandler(this.browseFiles_Click);
            // 
            // signButton
            // 
            this.signButton.Enabled = false;
            this.signButton.Location = new System.Drawing.Point(184, 161);
            this.signButton.Name = "signButton";
            this.signButton.Size = new System.Drawing.Size(151, 37);
            this.signButton.TabIndex = 6;
            this.signButton.Text = "Sign";
            this.signButton.UseVisualStyleBackColor = true;
            this.signButton.Click += new System.EventHandler(this.signButton_Click);
            // 
            // SignDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 210);
            this.Controls.Add(this.signButton);
            this.Controls.Add(this.browseFiles);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.outputLocation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.browseFile);
            this.Controls.Add(this.fileLocation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SignDocument";
            this.Text = "DocSign - Sign Document";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog fileDialog;
        private System.Windows.Forms.TextBox fileLocation;
        private System.Windows.Forms.Button browseFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FolderBrowserDialog folderDialog;
        private System.Windows.Forms.Button browseFiles;
        private System.Windows.Forms.TextBox outputLocation;
        private System.Windows.Forms.Button signButton;
    }
}